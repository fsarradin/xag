package xag

import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfterAll, FunSuiteLike, Matchers}

class XagMainTest extends FunSuiteLike with Matchers with BeforeAndAfterAll {

  import XagMainTest.spark
  import spark.implicits._

  override def afterAll(): Unit = {
    spark.stop()
    ()
  }

  test("should get max timestamp") {
    val df =
      Seq(("abc", 2L), ("def", 8L), ("ghi", 5L))
        .toDF("id", "timestamp")

    XagMain.getMaxTimestamp(df) should be(8)
  }

  test("should generate lookup") {
    val df =
      Seq("abc", "def", "abc", "ghi")
        .toDF("id")

    val result =
      XagMain.generateLookupOnColumn("id", df, spark)
        .collect()

    result.map(_ (1)) should (
      have size 3
        and contain allOf(0, 1, 2)
      )
  }

  test("should update ratings and filter input") {
    val oneDay = 1000L * 60 * 60 * 24 + 10000L
    val df =
      Seq(
        ("abc", 0.1D, 0L),
        ("def", 1.0D, oneDay)
      ).toDF("id", "rating", "timestamp")

    val result = XagMain.updateAndFilterRatings(df, oneDay)

    result.collect().map(_.toSeq) should equal (Array(Seq("def", 1.0, oneDay, 0, 1.0)))
  }

  test("should grouped input and sum ratings") {
    val df =
      Seq(
        ("abc", "1", 1.0),
        ("abc", "2", 1.0),
        ("abc", "1", 1.0),
        ("def", "1", 1.0)
      ).toDF("userId", "productId", "updatedRating")

    XagMain.groupedAndSumRatings(df).collect().map(_.toSeq) should contain only (
        Seq("abc", "1", 2.0),
        Seq("abc", "2", 1.0),
        Seq("def", "1", 1.0)
    )
  }

}

object XagMainTest {
  val spark: SparkSession =
    SparkSession
      .builder()
      .master("local[1]")
      .getOrCreate()

}