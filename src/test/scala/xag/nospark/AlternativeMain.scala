package xag.nospark

import java.io.{File, PrintWriter}
import java.time.temporal.ChronoUnit
import java.time.{Instant, LocalDateTime, ZoneId}

import scala.io.Source

/**
  * Experimental: work with 700MB file only.
  *
  */
object AlternativeMain {

  type Timestamp = Long

  def main(args: Array[String]): Unit = {
    // # lines = 11 922 520
    // # userId = 2 197 938
    // # productId = 316 999

    val directory = args(0)
    val inputFilename = directory + "xag.csv"
    val aggratingsFilename = directory + "aggratings.csv"
    val userLookupFilename = directory + "lookupuser.csv"
    val productLookupFilename = directory + "lookup_product.csv"

    val start = LocalDateTime.now()

    def updateLookup(lookup: Map[String, Long], id: String, max: Long): (Map[String, Long], Long, Long) =
      if (lookup.contains(id))
        (lookup, max, lookup(id))
      else
        (lookup + (id -> (max + 1L)), max + 1L, max + 1L)

    val output: Output =
      Source.fromFile(inputFilename)
        .getLines()
        .map(_.split(","))
        .map(a => RawInput(a(0), a(1), a(2).toDouble, a(3).toLong))
        .foldLeft(Output()) {
          case (acc, input) =>
            val (userLookup, maxUserInt, uid) = updateLookup(acc.userLookup, input.userId, acc.maxUserInt)
            val (productLookup, maxProductInt, pid) = updateLookup(acc.productLookup, input.productId, acc.maxProductInt)

            val newInput = IntIdInput(userId = uid, productId = pid, rating = input.rating, timestamp = input.timestamp)

            val key = (uid, pid)
            val inputLines =
              if (acc.inputLines.contains(key))
                acc.inputLines + (key -> (acc.inputLines.apply(key) :+ newInput))
              else
                acc.inputLines + (key -> (newInput :: Nil))

            Output(
              userLookup = userLookup,
              maxUserInt = maxUserInt,
              productLookup = productLookup,
              maxProductInt = maxProductInt,
              inputLines = inputLines,
              maxTimestamp = Math.max(acc.maxTimestamp, input.timestamp)
            )
        }

    val userLookup = output.userLookup
    val productLookup = output.productLookup
    val inputLines = output.inputLines
    val maxTimestamp = output.maxTimestamp

    println("user lookup table size: " + userLookup.size)
    println(output.maxUserInt)
    println("product lookup table size: " + productLookup.size)
    println(output.maxProductInt)
    println("grouped table size: " + inputLines.size)
    println("max timestamp: " + maxTimestamp)

    println("elapsed time: " + start.until(LocalDateTime.now(), ChronoUnit.MILLIS))

    lookupOutput(userLookupFilename, userLookup)
    lookupOutput(productLookupFilename, productLookup)

    println("elapsed time: " + start.until(LocalDateTime.now(), ChronoUnit.MILLIS))

    val newRatings: Map[(Long, Long), Double] =
      inputLines.mapValues(
        _
          .map(input => updateRating(input.rating, daysSpent(input.timestamp, maxTimestamp)))
          .filter(_ > 0.1)
          .sum
      )

    println("elapsed time: " + start.until(LocalDateTime.now(), ChronoUnit.MILLIS))

    val writer = new PrintWriter(new File(aggratingsFilename))
    newRatings.foreach {
      case ((uid, pid), rating) => writer.println(s"$uid,$pid,$rating")
    }
    writer.close()

    println("elapsed time: " + start.until(LocalDateTime.now(), ChronoUnit.MILLIS))

    /*
    Result on MBP 16GB 2.5 GHz Intel Core i7 2015:

user lookup table size: 2197938
2197938
product lookup table size: 316999
316999
product lookup table size: 10199041
max timestamp: 1477353599713
elapsed time: 60992
elapsed time: 62716
elapsed time: 62730
elapsed time: 80459
 */
  }

  def lookupOutput(filename: String, lookup: Map[String, Long]): Unit = {
    val writer = new PrintWriter(new File(filename))
    lookup.foreach { case (id, int) => writer.println(s"$id,$int") }
    writer.close()
  }

  def daysSpent(t1: Timestamp, t2: Timestamp): Long = {
    val dt1 = Instant.ofEpochMilli(t1).atZone(ZoneId.of("GMT")).toLocalDateTime
    val dt2 = Instant.ofEpochMilli(t2).atZone(ZoneId.of("GMT")).toLocalDateTime

    dt1.until(dt2, ChronoUnit.DAYS).abs
  }

  def updateRating(rating: Double, daysSpent: Long, penalty: Double = 0.95): Double =
    rating * Math.pow(penalty, daysSpent.toDouble)

}

case class Output(userLookup: Map[String, Long] = Map(),
                  maxUserInt: Long = 0L,
                  productLookup: Map[String, Long] = Map(),
                  maxProductInt: Long = 0L,
                  inputLines: Map[(Long, Long), List[IntIdInput]] = Map(),
                  maxTimestamp: Long = 0L)

case class RawInput(userId: String, productId: String, rating: Double, timestamp: Long)

case class IntIdInput(userId: Long, productId: Long, rating: Double, timestamp: Long)
