package xag

import java.time.temporal.ChronoUnit
import java.time.{LocalDateTime, ZoneId}

import org.scalatest.prop.Checkers
import org.scalatest.{FunSuiteLike, Matchers}

class AnalyserTest extends FunSuiteLike with Matchers with Checkers {

  test("should get same rate with no day spent") {
    Analyser.updateRating(1.0, 0, penalty = 0.95) should be (1.0 +- 1e-7)
  }

  test("should get diminished rate with one day spent") {
    Analyser.updateRating(1.0, 1, penalty = 0.95) should be (0.95 +- 1e-7)
  }

  test("should get doubly diminished rate with two days spent") {
    Analyser.updateRating(1.0, 2, penalty = 0.95) should be (0.9025 +- 1e-7)
  }

  test("property: should compute day spent between two timestamps") {
    check(
      (int: Int) => {
        val days = int.abs
        val end = LocalDateTime.now()
        val endts = end.atZone(ZoneId.of("GMT")).toInstant.toEpochMilli
        val start = end.minus(days.toLong, ChronoUnit.DAYS)
        val startts = start.atZone(ZoneId.of("GMT")).toInstant.toEpochMilli

        Analyser.daysSpent(startts, endts) == days
      }
    )
  }

  test("property: check daysSpent is a distance") {
    check(
      (int: Int) => {
        val days = int.abs
        val end = LocalDateTime.now()
        val endts = end.atZone(ZoneId.of("GMT")).toInstant.toEpochMilli
        val start = end.minus(days.toLong, ChronoUnit.DAYS)
        val startts = start.atZone(ZoneId.of("GMT")).toInstant.toEpochMilli

        Analyser.daysSpent(startts, endts) == Analyser.daysSpent(endts, startts)
      }
    )
  }

}
