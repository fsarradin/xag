package xag

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

object XagMain {

  private val inputSchema =
    StructType(
      StructField("userId", StringType)
        :: StructField("productId", StringType)
        :: StructField("rating", DoubleType)
        :: StructField("timestamp", LongType)
        :: Nil
    )

  /**
    *
    * @param args 0:master, 1:data_directory
    */
  def main(args: Array[String]): Unit = {
    val directory = args(1)
    val inputFilename = directory + "xag.csv"
    val aggratingsFilename = directory + "aggratings.csv"
    val userLookupFilename = directory + "lookupuser.csv"
    val productLookupFilename = directory + "lookup_product.csv"

    val spark =
      SparkSession
        .builder()
        .appName("XAG Main")
        .master(args(0))
        .getOrCreate()

    val inputDs = readInputCsv(spark, inputFilename).cache()

    val userLookup: DataFrame = generateLookupOnColumn("userId", inputDs, spark)
    val productLookup: DataFrame = generateLookupOnColumn("productId", inputDs, spark)

    val maxTimestamp: Long = getMaxTimestamp(inputDs)

    val newRatings = updateAndFilterRatings(inputDs, maxTimestamp)

    val result =
      groupedAndSumRatings(newRatings)
        .join(userLookup, "userId")
        .join(productLookup, "productId")
        .select("userIdAsInteger", "productIdAsInteger", "ratingSum")

    userLookup
      .coalesce(1)
      .write
      .format("com.databricks.spark.csv")
      .save(userLookupFilename)

    productLookup
      .coalesce(1)
      .write
      .format("com.databricks.spark.csv")
      .save(productLookupFilename)

    result
      .coalesce(1)
      .write
      .format("com.databricks.spark.csv")
      .save(aggratingsFilename)

    spark.stop()
  }

  def groupedAndSumRatings(newRatings: DataFrame): DataFrame =
    newRatings
      .groupBy("userId", "productId")
      .agg(sum("updatedRating").as("ratingSum"))

  def updateAndFilterRatings(inputDs: DataFrame, maxTimestamp: Long, threshold: Double = 0.1): DataFrame =
    inputDs
      .withColumn("daysSpent", daysSpentTo(maxTimestamp)(col("timestamp")))
      .withColumn("updatedRating", updateRating(col("rating"), col("daysSpent")))
      .where(col("updatedRating") > threshold)

  def readInputCsv(spark: SparkSession, inputFilename: String): DataFrame =
    spark.read
      .schema(inputSchema)
      .csv(inputFilename)

  def generateLookupOnColumn(colName: String, df: DataFrame, spark: SparkSession): DataFrame = {
    val rdd: RDD[Row] =
      df
        .select(col(colName))
        .distinct()
        .rdd
        .zipWithIndex()
        .map { case (row, index) => Row(row.getAs[String](colName), index) }

    val schema: StructType =
      StructType(
        StructField(colName, StringType)
          :: StructField(colName + "AsInteger", LongType)
          :: Nil
      )

    spark.createDataFrame(rdd, schema)
  }

  def getMaxTimestamp(df: DataFrame): Long = {
    val Row(maxTimestamp: Long) =
      df.agg(max("timestamp")).head()

    maxTimestamp
  }


  def daysSpentTo(t1: Long): UserDefinedFunction = udf[Long, Long](t2 => Analyser.daysSpent(t1, t2))

  val updateRating: UserDefinedFunction = udf[Double, Double, Long]((rating, daysSpent) => Analyser.updateRating(rating, daysSpent))

}
