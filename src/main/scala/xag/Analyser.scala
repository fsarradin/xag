package xag

import java.time.temporal.ChronoUnit
import java.time.{Instant, ZoneId}

object Analyser {

  /**
    * compute days spent between two timestamps.
    *
    * It is defined as a distance (`daysSpent(t1, t2) == daysSpent(t2, t1)`). Thus, parameter order is not important.
    *
    * @param t1
    * @param t2
    * @return
    */
  def daysSpent(t1: Long, t2: Long): Long = {
    val dt1 = Instant.ofEpochMilli(t1).atZone(ZoneId.of("GMT")).toLocalDateTime
    val dt2 = Instant.ofEpochMilli(t2).atZone(ZoneId.of("GMT")).toLocalDateTime

    dt1.until(dt2, ChronoUnit.DAYS).abs
  }

  /**
    * update a rating according to a number of days spent and by applying a multiplicative penalty.
    *
    * @param rating
    * @param days
    * @param penalty
    * @return
    */
  def updateRating(rating: Double, days: Long, penalty: Double = 0.95): Double =
    rating * Math.pow(penalty, days.toDouble)

}
